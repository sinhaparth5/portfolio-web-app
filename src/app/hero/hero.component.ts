import { Component } from '@angular/core';

@Component({
  selector: 'app-hero',
  standalone: true,
  imports: [],
  templateUrl: './hero.component.html',
  styleUrl: './hero.component.css'
})
export class HeroComponent {
  shape1Img: string = "assets/img/shape/shape-1.png";
  shape2Img: string = "assets/img/shape/shape-2.png";
  shape3Img: string = "assets/img/shape/shape-3.png";
  shape4Img: string = "assets/img/shape/shape-4.png";
  shape5Img: string = "assets/img/shape/shape-5.png";
  girlImg: string = "assets/img/home/girl-dark.png";
  heroShapeImg: string = "assets/img/shape/hero-shape-dark.png";
}
